package online.chenke.eurekaclientconsumer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "auth-server", fallback = UserFallBack.class)
public interface UserFeignClient {

    @GetMapping("/{username}/auth")
    public String authUser(@PathVariable("username") String username);
}
