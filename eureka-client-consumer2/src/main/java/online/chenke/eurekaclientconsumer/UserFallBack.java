package online.chenke.eurekaclientconsumer;

import org.springframework.stereotype.Component;

@Component
public class UserFallBack implements UserFeignClient {

    @Override
    public String authUser(String username) {
        return username + " login failed";
    }
}
