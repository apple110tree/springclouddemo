package online.chenke.eurekaclientconsumer;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserFeignClient userFeignClient;

    @GetMapping("/login/{username}")
    public String login(@PathVariable("username") String username) {
        System.out.println("I'm consumer");

        String result = userFeignClient.authUser(username);

        if ("success".equalsIgnoreCase(result)) {
            return (username + " login success");
        }

        return (username + " auth failed");
    }
}
