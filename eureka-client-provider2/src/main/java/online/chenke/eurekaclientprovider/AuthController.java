package online.chenke.eurekaclientprovider;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {
    @GetMapping("/{username}/auth")
    public String authUser(@PathVariable("username") String username) {
        System.out.println(username+" auth success");

        System.out.println("server 2 is servicing");

        return "success";
    }
}
